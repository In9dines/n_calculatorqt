#ifndef CALCULATORMAINWINDOW_H
#define CALCULATORMAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>

class CalculatorMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    CalculatorMainWindow(QWidget *parent = nullptr);
    ~CalculatorMainWindow();
    QLineEdit lineEdit;
    QLineEdit lineEdit_2;
    QLineEdit lineEdit_3;

public slots:
    void addPlus() { lineEdit_3.setText(lineEdit.text().toDouble() + lineEdit_2.text().toDouble()); }
    void addMinus() { lineEdit_3.setText(lineEdit.text().toDouble() - lineEdit_2.text().toDouble()); }
    void addMultiply() { lineEdit_3.setText(lineEdit.text().toDouble() * lineEdit_2.text().toDouble()); }
    void addDivide() { lineEdit_3.setText(lineEdit.text().toDouble() / lineEdit_2.text().toDouble()); }
private:
    Ui::CalculatorMainWindow *ui;
};
#endif // CALCULATORMAINWINDOW_H
